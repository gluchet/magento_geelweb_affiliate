# Magento Geelweb Affiliate

Affiliate platforms integration Magento module.

## Install

Download latest release from the Download page

    cd /tmp
    curl -O https://bitbucket.org/gluchet/magento_geelweb_affiliate/downloads/Geelweb_Affiliate-1.0.0.tgz

Install it using `./mage install-file`

    cd magento
    ./mage install-file /tmp/Geelweb_Affiliate-1.0.0.tgz

## Requirements

Magento >= 1.6

## Configuration

In Magento back-office go to `System > Configuration` then locate the
`Affiliation` section on the left panel.

### Effiliation

**Global configuration**

 -  Enable Catalogue Products XML feeds
 -  Enable Cross Canal feeds

**Catalogue products XML fields**

Define the fields to include in the xml file.

## Affiliate platforms

 - Effiliation http://your-shop-url/affiliate/effiliation/catalog
