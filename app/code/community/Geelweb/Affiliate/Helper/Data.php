<?php

class Geelweb_Affiliate_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function encoding($string, $to='ISO-8859-15', $from=null)
    {
        if ($from == null) {
            $from = mb_detect_encoding($string);
        }
        
        // try to fix malformated html entities (eg &eacute ;)
        $string = preg_replace('/&([^ ;]+) ?;/', '&$1;', $string);

	// and others non-existants fucking entities
	$string = str_replace(
		array('&eagrave;', '&rsquo;', ' &amp; '),
		array('&egrave;', '&apos;', ' et '),
		$string);

        // decode the html entities
        $string = strip_tags(html_entity_decode($string, ENT_COMPAT | ENT_HTML401, $from));

        if ($to == $from) {
            return $string;
        }

        return mb_convert_encoding($string, $to, $from);
    }
}
