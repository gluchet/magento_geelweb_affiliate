<?php

class Geelweb_Affiliate_Block_Effiliation_Catalog_Export extends Mage_Core_Block_Template
{
    private $_textFields = array(
        'delivery_time',
    );

    private $_ignore;

    protected function _toHtml()
    {
        $storeId = (int) $this->getRequest()->getParam('store', Mage::app()->getStore()->getId());

        $product = Mage::getModel('catalog/product');
        $products = $product->getCollection()
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('*');

        return $this->_renderXml($products);
    }

    protected function _renderXml($products)
    {
        $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>';
        $xml .= '<produits>';

        $data = $this->helper('geelweb_affiliate/data');

        foreach($products as $product) {
            if ($this->_ignoreProduct($product)) {
                continue;
            }
            $xml .= '<produit>';

            foreach ($this->_getProductAttributes() as $key => $attr) {
                if ($attr == 'none' || $key == 'exclude') {
                    continue;
                }

                if ($key == 'shipping_cost') {
                    $value = $this->_estimateShippingRate($product);
                } elseif (in_array($key, $this->_textFields)) {
                    $value = $attr;
                } else {
                    $value = $product->{$attr};
                }
                $xml .= "<$key>" . $data->encoding($value) . "</$key>";
            }

            // urls
            $xml .= '<url_product>' . $this->getUrl($product->getUrlPath()) . '</url_product>';
            $xml .= '<url_image>' . $this->helper('catalog/image')->init($product, 'small_image') . '</url_image>';

            // currency
            $currency = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getName();
            $xml .= '<currency>' . $currency . '</currency>';

            // in_stock
            $qtyStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
            $xml .= '<in_stock>' . $qtyStock . '</in_stock>';
            // availability
            // 001 = En stock ;
            // 002 = En réapprovisionnement ;
            // 003 = Dispo : voir site ;
            // 004 = En précommande ;
            // 005 = Disponible sur commande ;
            // 006 = Plus en fabrication
            $xml .= '<availability>' . ($qtyStock > 0 ? '001' : '003') . '</availability>';

            // merchant_*_name
            $xml .= $this->_getCategoriesXml($product);

            $xml .= '</produit>';
        }

        $xml .= '</produits>';
        return $xml;
    }

    protected function _getProductAttributes()
    {
        $config = Mage::getModel('geelweb_affiliate/export_config');
        $attributes = $config->getAttributes();
        return $attributes;
    }

    protected function _getCategoriesXml($product)
    {
        $tags = array(
            'merchant_store_name' => 1,
            'merchant_univers_name' => 2,
            'merchant_category_name' => 3,
            'merchant_department_name' => 4,
        );
        $xml = '';

        foreach ($tags as $tag => $level) {
            $categories = $product->getCategoryCollection()
                ->addFieldToFilter('level', $level)
                ->load();
            if ($categories->count()) {
                $category = array_pop($categories->getItems());
                $category = Mage::getModel('catalog/category')->load($category->getId());
                $value = $this->helper('geelweb_affiliate/data')->encoding($category->getName());
                $xml .= '<'.$tag.'>' . $value . '</' . $tag . '>';
            } else {
                $xml .= '<'.$tag.'></' . $tag . '>';
            }
        }

        return $xml;
    }

    protected function _ignoreProduct($product)
    {
        if (!$this->_ignore) {
            $data = Mage::getStoreConfig('effiliation_options/effiliation_fields/exclude');
            $this->_ignore = explode(',', $data);
        }

        return in_array($product->sku, $this->_ignore);
    }

    protected function _estimateShippingRate($product)
    {
        if ($product->isVirtual()) {
            return 0;
        }
        $quote = Mage::getModel('sales/quote');
        $quote->setIsSuperMode(true);
        $quote->getShippingAddress()->setCountryId(Mage::getStoreConfig('effiliation_options/effiliation_fields/shipping_country'));
        $quote->addProduct($product);

        $quote->getShippingAddress()->collectTotals();
        $quote->getShippingAddress()->setCollectShippingRates(true);
        $quote->getShippingAddress()->collectShippingRates();
        $rates = $quote->getShippingAddress()->getShippingRatesCollection();

        foreach ($rates as $rate) {
            return $rate->getPrice();
        }

        return 0;
    }
}
