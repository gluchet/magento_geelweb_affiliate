<?php

class Geelweb_Affiliate_EffiliationController extends Mage_Core_Controller_Front_Action
{
    public function catalogAction()
    {
        $format = $this->getRequest()->getParam('format', 'xml');
        if ($format != 'xml') {
            throw new Exception(sprintf("%s format not supported", $format));
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'text/xml; charset=ISO-8859-1');

        $this->loadLayout(false);
        $this->renderLayout();
    }
}
