<?php

class Geelweb_Affiliate_Model_Export_Config extends Varien_Object
{
    public function getAttributes($storeId = null)
    {
        return Mage::getStoreConfig('effiliation_options/effiliation_fields', $storeId);
    }
}
