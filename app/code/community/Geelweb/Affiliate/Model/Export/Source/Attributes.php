<?php

class Geelweb_Affiliate_Model_Export_Source_Attributes
{
    public function toOptionArray()
    {
        $attributes = Mage::getSingleton('geelweb_affiliate/export_convert_parser_product')->getExternalAttributes();
        array_unshift($attributes, array('value' => 'none', 'label' => 'Select an attribute'));
        return $attributes;
    }
}
